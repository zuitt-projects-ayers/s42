// Even Listeners
/*
	change		An element has been changed (e.g. input value).
	click		An element has been clicked (e.g. button clicks). 
	load		The browser has finished loading a webpage.
	keydown		A keyboard key is pushed.
	keyup		A keyboard key is let go (?).
*/

// In this line of code, we are getting the document in the HTML element with the id txt-first-name.

console.log(document);

const txtFirstName = document.querySelector("#txt-first-name");

// Alternative ways of targeting an element
	// document.getElementById("txt-first-name");
	// document.getElementByClassName();
	// document.getElementByTagName();

const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener('keyup', (event) => {

	// (innerHTML is the content between the opening and closing tags of an element.)
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {

	console.log(event.target);
	console.log(event.target.value);
});