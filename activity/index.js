const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

function createFullName() {

	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
	console.log(spanFullName.innerHTML);
}

txtFirstName.addEventListener('keyup', (event) => {

	createFullName()
});

txtLastName.addEventListener('keyup', (event) => {

	createFullName()
});
